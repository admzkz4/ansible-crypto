REPOSITORY_URL   := docker.repo.bab.net.au
RUNNER           := docker-compose run --rm
ANSIBLE_VAULT    := $(RUNNER) --entrypoint="ansible-vault" ansible
ANSIBLE_PLAYBOOK := $(RUNNER) --entrypoint="ansible-playbook" ansible
ANSIBLE_VAULTS   := $(patsubst vault/%.yaml,%,$(wildcard vault/*.yaml))

VAULT            ?= $(CI_ENVIRONMENT_NAME)

.SILENT:
.EXPORT_ALL_VARIABLES:

define operate_vault
	$(ANSIBLE_VAULT) $(1) vault/$(2).yaml
endef

vault_encrypt: _env _gen_prep _env-VAULT _check_vault_exists
	$(call operate_vault,encrypt,$(VAULT))
.PHONY: vault_encrypt

vault_decrypt: _env _gen_prep _env-VAULT _check_vault_exists
	$(call operate_vault,decrypt,$(VAULT))
.PHONY: vault_decrypt

vault_create: _env _env-ANSIBLE_VAULT_KEY _env-VAULT
	$(call operate_vault,create,$(VAULT))
.PHONY: vault_create

vault_reset: _env-VAULT
	git checkout -- vault/$(VAULT).yaml
.PHONY: vault_reset



#  -=-=-=-=-=-=-=-=-=-=-=-=-=- Utility helpers -=-=-=-=-=-=-=-=-=-=-=-=-
gen_%: _gen_prep _env-CI_ENVIRONMENT_NAME
	$(ANSIBLE_PLAYBOOK) scripts/generate.yaml \
		--extra-vars @vault/$(CI_ENVIRONMENT_NAME).yaml \
		--tags $@

_check_vault_exists: _env-VAULT
ifeq ($(filter $(VAULT),$(ANSIBLE_VAULTS)),)
	$(error Vault '$(VAULT)' does not exist. Only '$(ANSIBLE_VAULTS)' are valid options)
endif

_gen_prep: _env _env-ANSIBLE_VAULT_KEY
ifneq ("$(wildcard .vault_key)","")
else
	echo $(ANSIBLE_VAULT_KEY) > .vault_key
endif
.PHONY: _gen_prep


_env-%:
	if [ "${${*}}" = "" ]; then \
		echo "Environment variable $* not set"; \
		echo "Please check README.md for variables required"; \
		exit 1; \
	fi

_env:
	@cp .env.template .env
	@touch .env.assume
.PHONY: _env
