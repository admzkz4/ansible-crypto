## Ansible Crypto

To run the example, run the following

```bash
$ export ANSIBLE_VAULT_KEY=dfhf45b44kc
$ make gen_terraform CI_ENVIRONMENT_NAME=example
```

A file will be created in the root of the project called `generated.auto.tfvars` containing the terraform variables to use.

If you want to encrypt/decrypt the example vault run the following.

```bash
$ make vault_decrypt CI_ENVIRONMENT_NAME=example
```

```bash
$ make vault_encrypt CI_ENVIRONMENT_NAME=example
```

To create a new vault, run;

```bash
$ make vault_create CI_ENVIRONMENT_NAME=example
```

Stdin will be collected and you can enter the contents of a yaml document.
